#include "confirmationwidget.h"
#include "ui_confirmationwidget.h"

ConfirmationWidget::ConfirmationWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::ConfirmationWidget)
{
    ui->setupUi(this);
}

ConfirmationWidget::~ConfirmationWidget()
{
    delete ui;
}

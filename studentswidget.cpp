#include "studentswidget.h"
#include "ui_studentswidget.h"

StudentsWidget::StudentsWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::StudentsWidget)
{
    ui->setupUi(this);
    students_pushButton_list.append(ui->student_pushbutton_1);
    students_pushButton_list.append(ui->student_pushbutton_2);
    students_pushButton_list.append(ui->student_pushbutton_3);
    students_pushButton_list.append(ui->student_pushbutton_4);
    students_pushButton_list.append(ui->student_pushbutton_5);
    students_pushButton_list.append(ui->student_pushbutton_6);
    students_pushButton_list.append(ui->student_pushbutton_7);
    students_pushButton_list.append(ui->student_pushbutton_8);
    students_pushButton_list.append(ui->student_pushbutton_9);
    students_pushButton_list.append(ui->student_pushbutton_10);

    for(int i = 0; i< students_pushButton_list.size(); i++) connect(students_pushButton_list[i],SIGNAL(clicked(bool)),this,SLOT(studentPushButtonPressed()));
}

StudentsWidget::~StudentsWidget()
{
    delete ui;
}

void StudentsWidget::updateSize(){
    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);
}

void StudentsWidget::on_back_clicked()
{
   emit sendClose();
}

void StudentsWidget::populate(QJsonObject jsonObjData){
    offset = 0;
    jsonArrayStudents = jsonObjData["studentsList"].toArray();
    selectedStudents.clear();
    for(int i = 0; i < jsonArrayStudents.size(); i++){
        selectedStudents.append(false);
    }
    updateScreen();
}

void StudentsWidget::updateScreen(){
    for(int i = 0; i < 10; i++){
        students_pushButton_list[i]->setEnabled(false);
    }

    if(offset == 0) ui->pushButton_4->setEnabled(false);
    else ui->pushButton_4->setEnabled(true);

    if(offset + 10 > jsonArrayStudents.size()) ui->pushButton_5->setEnabled(false);
    else ui->pushButton_5->setEnabled(true);

    for(int i = offset; i < jsonArrayStudents.size() && i < offset+10; i++){
        students_pushButton_list[i - offset]->setEnabled(true);
        students_pushButton_list[i - offset]->setText(jsonArrayStudents[i].toObject()["name"].toString() + " " + jsonArrayStudents[i].toObject()["last_name"].toString());
        students_pushButton_list[i - offset]->setProperty("selected",selectedStudents[i]);
    }

    this->setStyleSheet(this->styleSheet());

}

void StudentsWidget::studentPushButtonPressed(){
    int studentElement = QObject::sender()->property("buttonOffset").toInt() + offset;
    if(selectedStudents[studentElement]) selectedStudents[studentElement] = false;
    else selectedStudents[studentElement] = true;
    updateScreen();
}

void StudentsWidget::on_pushButton_4_clicked()
{
    offset -= 10;
    updateScreen();
}

void StudentsWidget::on_pushButton_5_clicked()
{
    offset += 10;
    updateScreen();
}

void StudentsWidget::on_pushButton_clicked()
{
    for(int i = 0; i < selectedStudents.size(); i++) selectedStudents[i] = false;
    updateScreen();
}

void StudentsWidget::on_pushButton_3_clicked()
{

    for(int i = 0; i < selectedStudents.size(); i++) selectedStudents[i] = true;
    updateScreen();
}

void StudentsWidget::on_pushButton_2_clicked()
{

    for(int i = 0; i < selectedStudents.size(); i++) selectedStudents[i] = jsonArrayStudents[i].toObject()["isActual"].toBool();
    updateScreen();
}

void StudentsWidget::on_pushButton_6_clicked()
{
    QJsonArray jsonArray;
    for(int i = 0; i < jsonArrayStudents.size(); i++){
        if(selectedStudents[i]) jsonArray.append(jsonArrayStudents[i].toObject()["id"]);
    }
    emit sendStudents(jsonArray);
}

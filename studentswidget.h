#ifndef STUDENTSWIDGET_H
#define STUDENTSWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>
#include <QDesktopWidget>
#include <QPushButton>

namespace Ui {
class StudentsWidget;
}

class StudentsWidget : public QWidget
{
    Q_OBJECT

public:
    explicit StudentsWidget(QWidget *parent = 0);
    ~StudentsWidget();
    void populate(QJsonObject jsonObjData);
    void setUserLabel(QString userLabel);
    void updateStyleSheet(QString stylesheet);
    void updateSize();

signals:
    void sendClose();
    void sendStudents(QJsonArray jsonArray);
    
private slots:
    void on_back_clicked();
    void updateScreen();
    void studentPushButtonPressed();

    void on_pushButton_4_clicked();

    void on_pushButton_5_clicked();

    void on_pushButton_clicked();

    void on_pushButton_3_clicked();

    void on_pushButton_2_clicked();

    void on_pushButton_6_clicked();

private:
    Ui::StudentsWidget *ui;
    QJsonArray jsonArrayStudents;
    int offset;
    QList <QPushButton*> students_pushButton_list;
    QList <bool> selectedStudents;

};

#endif // STUDENTSWIDGET_H

#ifndef CONFIRMATIONWIDGET_H
#define CONFIRMATIONWIDGET_H

#include <QWidget>
#include <QJsonArray>
#include <QJsonObject>

namespace Ui {
class ConfirmationWidget;
}

class ConfirmationWidget : public QWidget
{
    Q_OBJECT

public:
    explicit ConfirmationWidget(QWidget *parent = 0);
    ~ConfirmationWidget();
    void populate(QJsonObject jsonObjData);
    void updateSize();
    void updateStyleSheet(QString stylesheet);

private:
    Ui::ConfirmationWidget *ui;

signals:
    void sendClose();
};



#endif // CONFIRMATIONWIDGET_H

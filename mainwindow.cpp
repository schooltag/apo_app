#include "mainwindow.h"
#include "ui_mainwindow.h"

int min(int a,int b){
    if(a < b) return a;
    else return b;
}

int max(int a,int b){
    if(a > b) return a;
    else return b;
}

MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    apId = "no_apid_yet";
    deviceId = "no_deviceId_yet";

    ui->setupUi(this);

    ui->centralWidgetMain->resize(QDesktopWidget().width(),QDesktopWidget().height());

    loginWidget = new LoginWidget(ui->centralWidgetMain);
    studentWidget = new StudentsWidget(ui->centralWidgetMain);
    infoWidget = new InfoWidget(ui->centralWidgetMain);

    loginWidget->hide();
    studentWidget->hide();
    infoWidget->hide();

    connect(infoWidget,SIGNAL(sendClose()),this,SLOT(infoClosed()));
    connect(studentWidget,SIGNAL(sendClose()),this,SLOT(studentsClosed()));
    connect(studentWidget,SIGNAL(sendStudents(QJsonArray)),this,SLOT(students(QJsonArray)));


    socket = new QTcpSocket();
    timerInit = new QTimer();
    connect(timerInit,SIGNAL(timeout()),this,SLOT(timerInitTimeout()));
    connect(socket,SIGNAL(readyRead()),this,SLOT(readyRead()));
    connect(socket,SIGNAL(connected()),this,SLOT(connected()));
    connect(socket,SIGNAL(disconnected()),this,SLOT(disconnected()));

    loadingGif = new QMovie();
    loadingGif->setFileName(":/res/loading.gif");
    ui->loadingGifLabel->setMovie(loadingGif);
    ui->loadingGifLabel->setGeometry(QDesktopWidget().width()/2 - 100,QDesktopWidget().height()/2 - 100,200,200);
    ui->refreshButton->resize(QDesktopWidget().size());

    receptionTimer = new QTimer();
    connect(receptionTimer,SIGNAL(timeout()),this,SLOT(receptionTimerTimeout()));

    tryToConnect();

}

MainWindow::~MainWindow()
{
    delete ui;
}



void MainWindow::connected(){

    QJsonObject jsonObjMain,jsonObjData;
    QString mac;

#ifdef Q_OS_WIN
    mac = QNetworkInterface::interfaceFromName("wireless_0").hardwareAddress();
#else //Q_OS_ANDROID
    mac = QNetworkInterface::interfaceFromName("wlan0").hardwareAddress();
#endif

    jsonObjData["deviceMac"] = mac;

    jsonObjMain["type"] = "ap_init";
    jsonObjMain["data"] = jsonObjData;

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());

}

void MainWindow::tryToConnect(){

    QHostAddress ip;
    QNetworkInterface ni;
    QString ipString,labelText;
    unsigned int puerto;
    loadingGif->start();
    ui->loadingGifLabel->show();
    ui->refreshButton->hide();

#ifdef Q_OS_WIN
     ni = QNetworkInterface::interfaceFromName("wireless_0");
     puerto = 5740;
#else //Q_OS_ANDROID
    ni = QNetworkInterface::interfaceFromName("wlan0");
    puerto = 5790;
#endif

    for(int i = 0; i < ni.addressEntries().size(); i++){
        ip = ni.addressEntries()[i].ip();
        if(ip.protocol() == QAbstractSocket::IPv4Protocol) break;
    }

    ipString =  ip.toString().split('.')[0] + "." +
                ip.toString().split('.')[1] + "." +
                ip.toString().split('.')[2] + "." +
                "50";                                   //   <---- CAMBIAR A DISCRECIÓN
                //ip.toString().split('.')[3];


#ifdef Q_OS_WIN
    socket->connectToHost(QHostAddress::LocalHost,puerto);
#else //Q_OS_ANDROID
    socket->connectToHost(QHostAddress(ipString),puerto);
#endif

    labelText = QString::number(QDesktopWidget().height()) + "x" + QString::number(QDesktopWidget().width()) + ", " +
                ipString + ":"  + QString::number(puerto)+ ", (Local: " + ni.hardwareAddress() + ", " + ip.toString() + ")";

    ui->label->setText(labelText);

    timerInit->start(6000);
}

void MainWindow::timerInitTimeout(){
    socket->blockSignals(true);
    socket->abort();
    socket->blockSignals(false);
    ui->refreshButton->show();
    ui->loadingGifLabel->hide();
    loadingGif->stop();
}

void MainWindow::disconnected(){
    loginWidget->hide();
    infoWidget->hide();
    studentWidget->hide();
    tryToConnect();
}

void MainWindow::receptionTimerTimeout(){
    receptionBuffer.clear();
}

void MainWindow::readyRead(){
    receptionTimer->start(500);
    QByteArray qba = socket->readAll();

    receptionBuffer.append(qba);

    QJsonObject jsonObjData,jsonObjMain = QJsonDocument::fromJson(receptionBuffer).object();
    int r,g,b;

    QString jsonType = jsonObjMain["type"].toString();

    if(jsonType == "ap_appInit"){
        timerInit->stop();

        resize(QDesktopWidget().width(),QDesktopWidget().height());
        ui->centralWidgetMain->resize(QDesktopWidget().width(),QDesktopWidget().height());

        loginWidget->updateSize();
        studentWidget->updateSize();
        infoWidget->updateSize();


        jsonObjData = jsonObjMain["data"].toObject();

        deviceId = jsonObjData["deviceId"].toString();


        r = jsonObjData["palette"].toObject()["base"].toObject()["r"].toInt();
        g = jsonObjData["palette"].toObject()["base"].toObject()["g"].toInt();
        b = jsonObjData["palette"].toObject()["base"].toObject()["b"].toInt();

        baseColor = "rgb(" + QString::number(r) + ',' + QString::number(g) + ',' + QString::number(b) + ')';
        baseDarkColor = "rgb(" + QString::number(max(r - 25,0)) + ',' + QString::number(max(r - 25,0)) + ',' + QString::number(max(r - 25,0)) + ')';
        baseLightColor = "rgb(" + QString::number(min(r + 25,255)) + ',' + QString::number(min(r + 25,255)) + ',' + QString::number(min(r + 25,255)) + ')';

        r = jsonObjData["palette"].toObject()["secondary"].toObject()["r"].toInt();
        g = jsonObjData["palette"].toObject()["secondary"].toObject()["g"].toInt();
        b = jsonObjData["palette"].toObject()["secondary"].toObject()["b"].toInt();

        secondaryColor = "rgb(" + QString::number(r) + ',' + QString::number(g) + ',' + QString::number(b) + ')';
        secondaryDarkColor = "rgb(" + QString::number(max(r - 25,0)) + ',' + QString::number(max(r - 25,0)) + ',' + QString::number(max(r - 25,0)) + ')';
        secondaryLightColor = "rgb(" + QString::number(min(r + 25,255)) + ',' + QString::number(min(r + 25,255)) + ',' + QString::number(min(r + 25,255)) + ')';

        r = jsonObjData["palette"].toObject()["trim"].toObject()["r"].toInt();
        g = jsonObjData["palette"].toObject()["trim"].toObject()["g"].toInt();
        b = jsonObjData["palette"].toObject()["trim"].toObject()["b"].toInt();

        trimColor = "rgb(" + QString::number(r) + ',' + QString::number(g) + ',' + QString::number(b) + ')';

        loadingGif->stop();
        setStyleSheets();

        studentWidget->hide();
        infoWidget->hide();

        loginWidget->show();

        return;
    }

    if(jsonType == "ap_studentsList"){
        jsonObjMain = jsonObjMain["data"].toObject();
        apId = jsonObjMain["apId"].toString();
        studentWidget->populate(jsonObjMain);
        studentWidget->show();
        loginWidget->hide();
        infoWidget->hide();
        return;
    }

    if(jsonType == "ap_info"){
        jsonObjMain = jsonObjMain["data"].toObject();
        infoWidget->populate(jsonObjMain);
        infoWidget->show();
        loginWidget->hide();
        studentWidget->hide();
        return;
    }
}

void MainWindow::ci(QString ciString){
    QJsonObject jsonObjMain,jsonObjData;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["ciData"] = ciString;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "ap_ci";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());
}

void MainWindow::students(QJsonArray jsonArray){
    QJsonObject jsonObjMain,jsonObjData;

    jsonObjData["deviceId"] = deviceId;
    jsonObjData["roleId"] = apId;
    jsonObjData["studentsIdList"] = jsonArray;

    jsonObjMain["data"] = jsonObjData;
    jsonObjMain["type"] = "ap_studentSelection";

    QJsonDocument jsonDoc(jsonObjMain);
    socket->write(jsonDoc.toJson());
}

void MainWindow::setStyleSheets(){


    /*QString stylesheetString = "QFrame{background-color: " + baseColor + ";} \
            QPushButton{outline: none;border: 2px  solid ;border-radius: 10px;border-color: " + trimColor + ";color: " + trimColor + ";background-color: " + secondaryColor + ";} \
            QPushButton:checked{background-color: " + baseLightColor + ";} \
            QPushButton:disabled{background-color: " + baseColor + ";color: " + secondaryDarkColor + ";border-color: " + secondaryDarkColor + ";} \
            QComboBox {border: 2px  solid ;border-radius: 10px;border-color: " + trimColor + ";color: " + trimColor + ";background-color: " + secondaryColor + ";padding-left: 5px;} \
            QComboBox::drop-down {subcontrol-origin: padding;subcontrol-position: top right;width: 40px;border-left-width: 2px;border-left-color: " + trimColor + " ;border-left-style: solid;border-top-right-radius: 3px;border-bottom-right-radius: 3px;} \
            QComboBox::down-arrow {image: url(:/res/down-arrow.png);} \
            QComboBox QAbstractItemView {border: 2px solid darkgray;selection-background-color:  " + secondaryDarkColor + ";color: " + trimColor + ";background-color: " + secondaryLightColor + ";color: " + trimColor + ";padding-left: 5px;} \
            QLabel{color: " + trimColor + ";} \
            #help{background-image: url(:/res/question-mark.png);background-repeat: no-repeat;background-position: center;border-color: " + baseColor + "; background-color: " + baseColor + ";} \
            #back{background-image: url(:/res/left-arrow.png);background-repeat: no-repeat;background-position: center;border-color: " + baseColor + "; background-color: " + baseColor + ";} \
            #logo300{background-image: url(:/res/Smiley-300x300.png);background-repeat: no-repeat;background-position: center;border-color: " + baseColor + "; background-color: " + baseColor + ";}  \
            QFrame#overlay{background-color: rgba(254, 0, 0, 255);} \
            #dialogFrame{background-color: " + baseColor + "; border: 2px  solid ;border-radius: 10px;border-color: " + trimColor + ";}\
            QPushButton[studentButton=true]{border:none;background-repeat: no-repeat;background-color: " + baseColor + ";background-position:  left center;background-origin: border;text-align: left;padding-left: 50px;}\
            QPushButton[status=\"called\"][male=true]{background-image: url(:/res/user_male_called.png);}\
            QPushButton[status=\"absent\"][male=true]{background-image: url(:/res/user_male_absent.png);}\
            QPushButton[status=\"present\"][male=true]{background-image: url(:/res/user_male_present.png);}\
            QPushButton[status=\"called\"][male=false]{background-image: url(:/res/user_female_called.png);}\
            QPushButton[status=\"absent\"][male=false]{background-image: url(:/res/user_female_absent.png);}\
            QPushButton[status=\"present\"][male=false]{background-image: url(:/res/user_female_present.png);}";

*/

    QString stylesheetString = "QFrame{background-color: " + baseColor + ";} \
            QFrame#secondFrame{background-color: " + secondaryColor + ";} \
            QPushButton{border-style: solid;border-width: 5px; border-top-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 " + baseColor + ", stop:1 " + secondaryColor + ");border-bottom-color: qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 " + baseColor + ", stop:1 " + secondaryColor + ");border-left-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 " + baseColor + ", stop:1 " + secondaryColor + ");border-right-color: qlineargradient(spread:pad, x1:1, y1:0, x2:0, y2:0, stop:0 " + baseColor + ", stop:1 " + secondaryColor + ");background-color: " + secondaryColor + ";color: " + trimColor + ";}\
            QPushButton:checked{border-style: solid;border-width: 5px;border-top-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");border-bottom-color: qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");border-left-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");border-right-color: qlineargradient(spread:pad, x1:1, y1:0, x2:0, y2:0, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");background-color: " + baseLightColor + ";color: " + trimColor + ";}\
            QPushButton:disabled{border-style: solid;border-width: 5px; border-top-color: qlineargradient(spread:pad, x1:0, y1:0, x2:0, y2:1, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");border-bottom-color: qlineargradient(spread:pad, x1:0, y1:1, x2:0, y2:0, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");border-left-color: qlineargradient(spread:pad, x1:0, y1:0, x2:1, y2:0, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");border-right-color: qlineargradient(spread:pad, x1:1, y1:0, x2:0, y2:0, stop:0 " + baseColor + ", stop:1 " + baseLightColor + ");background-color: " + baseColor + ";color: " + baseLightColor + ";} \
            QComboBox {border: 2px  solid ;border-radius: 10px;border-color: " + trimColor + ";color: " + trimColor + ";background-color: " + secondaryColor + ";padding-left: 5px;} \
            QComboBox::drop-down {subcontrol-origin: padding;subcontrol-position: top right;width: 40px;border-left-width: 2px;border-left-color: " + trimColor + " ;border-left-style: solid;border-top-right-radius: 3px;border-bottom-right-radius: 3px;} \
            QComboBox::down-arrow {image: url(:/res/down-arrow.png);} \
            QComboBox QAbstractItemView {border: 2px solid darkgray;selection-background-color:  " + secondaryDarkColor + ";color: " + trimColor + ";background-color: " + secondaryLightColor + ";color: " + trimColor + ";padding-left: 5px;} \
            QLabel{color: " + trimColor + "; background-color: transparent;} \
            #help{background-image: url(:/res/question-mark.png);background-repeat: no-repeat;background-position: center;border-color: transparent; background-color: transparent;} \
            #back{background-image: url(:/res/left-arrow.png);background-repeat: no-repeat;background-position: center;border-color: transparent; background-color: transparent;} \
            #logo300{background-image: url(:/res/Smiley-300x300.png);background-repeat: no-repeat;background-position: center;border-color: " + baseColor + "; background-color: " + baseColor + ";}  \
            QFrame#overlay{background-color: rgba(0, 0, 0, 192);} \
            #dialogFrame{background-color: " + baseColor + "; border: 2px  solid ;border-radius: 10px;border-color: " + trimColor + ";} \
            QPushButton[studentPushButton=true]{border-style: none;background-color: transparent;background-repeat: no-repeat;background-position:  left center;color: " + trimColor + ";background-origin: border;text-align: left;padding-left: 50px;}\
            QPushButton[studentPushButton=true][selected=true]:enabled{background-image: url(:/res/user_male_present.png);font-weight: bold;}\
            QPushButton[studentPushButton=true][selected=false]:enabled{background-image: url(:/res/user_male_absent.png);font-weight: normal;}\
            QPushButton[studentPushButton=true]:disabled{background-color: transparent;color: transparent;}";

            loginWidget->setStyleSheet(stylesheetString);
            studentWidget->setStyleSheet(stylesheetString);
            infoWidget->setStyleSheet(stylesheetString);


}

void MainWindow::infoClosed(){
    loginWidget->show();
    studentWidget->hide();
    infoWidget->hide();
}

void MainWindow::studentsClosed(){
    loginWidget->show();
    studentWidget->hide();
    infoWidget->hide();
}



void MainWindow::on_refreshButton_clicked()
{
    tryToConnect();
}

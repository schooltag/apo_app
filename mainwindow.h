#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTcpSocket>
#include <QHostAddress>
#include <QJsonArray>
#include <QJsonDocument>
#include <QJsonObject>
#include <QTimer>
#include <QMovie>
#include <QDesktopWidget>
#include <QNetworkInterface>
#include "loginwidget.h"
#include "studentswidget.h"
#include "infowidget.h"

namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT
public slots:
    void readyRead();
    void connected();
    void disconnected();
    void timerInitTimeout();
    void receptionTimerTimeout();
    void infoClosed();
    void studentsClosed();
public:
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();

private slots:
    void on_refreshButton_clicked();
    void students(QJsonArray jsonArray);
    void ci(QString ciString);

private:
    Ui::MainWindow *ui;
    QTcpSocket* socket;
    QString baseColor,secondaryColor,baseDarkColor,secondaryDarkColor,trimColor,baseLightColor,secondaryLightColor;
    void setStyleSheets();
    QTimer* timerInit,* receptionTimer;
    void tryToConnect();
    QMovie* loadingGif;
    QByteArray receptionBuffer;
    QString deviceId,apId;
    LoginWidget* loginWidget;
    StudentsWidget* studentWidget;
    InfoWidget* infoWidget;

};

#endif // MAINWINDOW_H

#ifndef LOGINWIDGET_H
#define LOGINWIDGET_H

#include <QWidget>
#include <QTimer>
#include <QDate>
#include <QTime>
#include <QDesktopWidget>

namespace Ui {
class LoginWidget;
}

class LoginWidget : public QWidget
{
    Q_OBJECT

public:
    explicit LoginWidget(QWidget *parent = 0);
    ~LoginWidget();
    void updateSize();
    void updateStyleSheet(QString stylesheet);

private:
    Ui::LoginWidget *ui;
    QTimer* clockTimer;
public slots:
    void clockTimerTic();
};



#endif // LOGINWIDGET_H

#ifndef INFOWIDGET_H
#define INFOWIDGET_H

#include <QWidget>
#include <QJsonObject>
#include <QTimer>
#include <QDesktopWidget>

namespace Ui {
class InfoWidget;
}

class InfoWidget : public QWidget
{
    Q_OBJECT

public:
    explicit InfoWidget(QWidget *parent = 0);
    ~InfoWidget();
    void updateSize();
    void populate(QJsonObject jsonObjData);
    void show();

private slots:
    void timeout();

signals:
    void sendClose();

private:
    Ui::InfoWidget *ui;
    QTimer* timer;
    int showTimer;
};

#endif // INFOWIDGET_H

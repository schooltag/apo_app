#include "infowidget.h"
#include "ui_infowidget.h"

InfoWidget::InfoWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::InfoWidget)
{
    ui->setupUi(this);
    timer = new QTimer();
    showTimer = 5000;
    connect(timer,SIGNAL(timeout()),this,SLOT(timeout()));

}

InfoWidget::~InfoWidget()
{
    delete ui;
}

void InfoWidget::populate(QJsonObject jsonObjData){
    int level = jsonObjData["level"].toString().toInt();
    QString text = jsonObjData["text"].toString();
    QString title = jsonObjData["title"].toString();
    if(jsonObjData["time"] == QJsonValue::Undefined || jsonObjData["time"].toString().toInt() == 0) showTimer = 5000;
    else showTimer = jsonObjData["time"].toString().toInt();


    switch(level){
    case 1:
        ui->colorFrame->setStyleSheet("background-color: rgb(0, 170, 0);");
        ui->infoIcon->setStyleSheet("background-image: url(:/res/rounded-info-button.png);");
        break;
    case 2:
        ui->colorFrame->setStyleSheet("background-color: rgb(0, 85, 255);");
        ui->infoIcon->setStyleSheet("background-image: url(:/res/rounded-info-button.png);");
        break;
    case 3:
        ui->colorFrame->setStyleSheet("background-color: rgb(225, 225, 0);");
        ui->infoIcon->setStyleSheet("background-image: url(:/res/danger.png);");
        break;
    case 4:
        ui->colorFrame->setStyleSheet("background-color: rgb(255, 0, 0);");
        ui->infoIcon->setStyleSheet("background-image: url(:/res/danger.png);");
        break;
    default:
        ui->colorFrame->setStyleSheet("background-color: rgb(0, 85, 255);");
        ui->infoIcon->setStyleSheet("background-image: url(:/res/rounded-info-button.png);");
        break;
    }

    ui->title_label->setText(title);
    ui->text_label->setText(text);
}

void InfoWidget::show(){
    timer->start(showTimer);
    QWidget::show();
}

void InfoWidget::timeout(){
    emit sendClose();
}

void InfoWidget::updateSize(){
    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);
}

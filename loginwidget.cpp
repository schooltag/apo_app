#include "loginwidget.h"
#include "ui_loginwidget.h"

LoginWidget::LoginWidget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::LoginWidget)
{
    ui->setupUi(this);

    clockTimer = new QTimer(this);
    connect(clockTimer,SIGNAL(timeout()),this,SLOT(clockTimerTic()));
    clockTimer->start(1000);
}


LoginWidget::~LoginWidget()
{
    delete ui;
}

void LoginWidget::clockTimerTic(){
    QTime time = QTime::currentTime();
    QString hora = time.toString("H:mm");
    //if((time.second() % 2) == 0) hora[2] = ' ';

    QString dia = QDate::currentDate().toString("ddd");

    if(dia == "Mon" || dia == "lun") dia = "Lunes";
    else if(dia == "Tue" || dia == "mar") dia = "Martes";
    else if(dia == "Wed" || dia == "mie") dia = "Miércoles";
    else if(dia == "Thu" || dia == "jue") dia = "Jueves";
    else if(dia == "Fri" || dia == "vie") dia = "Viernes";
    else if(dia == "Sat" || dia == "sab") dia = "Sábado";
    else if(dia == "Sun" || dia == "dom") dia = "Domingo";
    else dia = "WTF";

    QString mes;

    switch(QDate::currentDate().toString("M").toUInt()){
       case 1: mes = "Enero"; break;
       case 2: mes = "Febrero"; break;
       case 3: mes = "Marzo"; break;
       case 4: mes = "Abril"; break;
       case 5: mes = "Mayo"; break;
       case 6: mes = "Junio"; break;
       case 7: mes = "Julio"; break;
       case 8: mes = "Agosto"; break;
       case 9: mes = "Septiembre"; break;
       case 10: mes = "Octubre"; break;
       case 11: mes = "Noviembre"; break;
       case 12: mes = "Diciembre"; break;
       default: dia = "Smarch"; break;
    }

    QString numero = QString::number(QDate::currentDate().toString("d").toUInt());

    ui->horaLabel->setText(hora);
    ui->diaLabel->setText(dia);
    ui->fechaLabel->setText(numero + " de " + mes);

}

void LoginWidget::updateSize(){
    int width = QDesktopWidget().width();
    int height = QDesktopWidget().height();

    resize(width,height);
    ui->frame->resize(width,height);
}
